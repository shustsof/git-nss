# Monster High Dolls

Monster High is a popular doll line created by Mattel. The dolls are known for their unique, edgy designs inspired by monster movies, sci-fi horror, and various creatures from folklore. Each doll represents a character with a distinctive personality and backstory, often involving being the offspring of famous monsters. The franchise also includes web series, movies, and books, creating a rich universe that appeals to children and collectors alike.

## Table of Contents

1. [Application Name](#application-name)
2. [List of Authors](#list-of-authors)
3. [List of Technologies](#list-of-technologies)
4. [Description](#description)
5. [Installation Name](#installation-name)
6. [Copyright](#copyright)

---

### Application Name

Monster High Dolls

### List of Authors

- Mattel
- Garrett Sander (Designer)
- Kellee Riley (Designer)
- Rebecca Shipman (Designer)

### List of Technologies

- Plastic and Fabric Materials
- Fashion Design
- Animation Software (for web series and movies)
- Print Media (for books and packaging)

### Description

Monster High is a franchise that includes a series of fashion dolls, an animated web series, movies, and various merchandise. The dolls are characterized by their unique, often gothic or punk-inspired aesthetics, representing teenage children of famous monsters like Dracula, Frankenstein, and the Mummy. The franchise promotes themes of acceptance, diversity, and friendship, appealing to both children and adult collectors.

### Installation Name

Monster High Installation

### Copyright

© 2024 Mattel, Inc. All Rights Reserved.